package com.example.ars.liveapppresentation;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

class DBHelper extends SQLiteOpenHelper
{
    public DBHelper(Context context) {
        super(context, "dataBase", null, R.integer.db_version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table states ("
                + "state int,"
                + "time string"
                + ");");

        db.execSQL("create table appStates ("
                + "message string,"
                + "time string"
                + ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
