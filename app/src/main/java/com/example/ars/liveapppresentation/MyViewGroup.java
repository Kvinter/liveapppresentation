package com.example.ars.liveapppresentation;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Build;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.annotation.StyleRes;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;

/**
 * Created by Ars on 08.04.2017.
 */
class MyViewGroup extends FrameLayout
{

    private boolean isRunMeasure = false;

    private ArrayAdapter<String> arrayAdapter;

    public MyViewGroup(@NonNull Context context) {
        super(context);
    }

    public MyViewGroup(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public MyViewGroup(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public MyViewGroup(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr, @StyleRes int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }


    public void setArrayAdapter(ArrayAdapter<String> arrayAdapter)
    {
        this.arrayAdapter = arrayAdapter;
    }


    private void addText(String str)
    {
        if(arrayAdapter != null) {
            arrayAdapter.add(str);
            arrayAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void addView(View child) {
        addText(String.format("К ViewGroup добавлен View: %s", child.toString()));
        super.addView(child);
    }

    @Override
    public void addView(View child, int width, int height) {
        addText(String.format("К ViewGroup добавлен View: %s", child.toString()));
        super.addView(child, width, height);
    }

    @Override
    public void addView(View child, ViewGroup.LayoutParams params) {
        addText(String.format("К ViewGroup добавлен View: %s", child.toString()));
        super.addView(child, params);
    }

    @Override
    public void removeView(View view) {
        addText(String.format("Из ViewGroup удален View: %s", view.toString()));
        super.removeView(view);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        //Вызываем только раз, иначе лог засорится
        if(!isRunMeasure) {
            addText("Размер ViewGroup и его детей были расчитаны");
            isRunMeasure = true;
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        addText("ViewGroup взято из XML");
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        addText("ViewGroup прикреплено к окну");
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        if(changed)
            addText(String.format("ViewGroup назначил размер и положение для потомков\nleft:%s top:%s right:%s bottom:%s",
                    l, t, r, b));
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        addText(String.format("Размер ViewGroup изменен (Old(W:%s H:%s), New(W:%s H:%S))",oldw, oldh, w, h ));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        addText("Отрисовка ViewGroup");
    }
}
