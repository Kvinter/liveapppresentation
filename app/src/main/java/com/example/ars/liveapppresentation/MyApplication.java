package com.example.ars.liveapppresentation;

import android.app.Application;
import android.content.ContentValues;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.ArrayAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class MyApplication extends Application
{
    private DBHelper dbHelper;
    private ArrayAdapter<MessageItem> arrayAdapter;
    private SimpleDateFormat sdf;
    ArrayList<MessageItem> temporaryList;

    public MyApplication()
    {
        sdf = new SimpleDateFormat("HH:mm:ss.SSS");
        temporaryList = new ArrayList<>();
    }

    public void Clear()
    {
        temporaryList = new ArrayList<>();
        arrayAdapter.clear();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.delete("appStates", null, null);
        arrayAdapter.notifyDataSetChanged();
    }
    private void reestablish(SQLiteDatabase db)
    {
        Cursor c = db.query("appStates", null, null, null, null, null, null);
        if(!c.moveToFirst()) return;
        int statColIndex = c.getColumnIndex("message");
        int timeColIndex = c.getColumnIndex("time");

        do {
            MessageItem item = new MessageItem(c.getString(statColIndex), c.getString(timeColIndex));
            if(arrayAdapter != null)
                arrayAdapter.add(item);
            else
                temporaryList.add(item);
        } while(c.moveToNext());
    }

    public void setDbHelper(DBHelper dbHelper)
    {
        this.dbHelper = dbHelper;
        reestablish(dbHelper.getWritableDatabase());
        for (MessageItem messageItem : temporaryList)
        {
            addItemToDB(messageItem);
        }
    }

    public void setArrayAdapter(ArrayAdapter<MessageItem> arrayAdapter)
    {
        this.arrayAdapter = arrayAdapter;
        arrayAdapter.addAll(temporaryList);
    }

    private void addItemToDB(MessageItem item)
    {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put("message", item.Message);
        cv.put("time", item.Time);

        db.insert("appStates", null, cv);
    }

    private void addText(String str)
    {
        Date date = new Date();
        MessageItem item = new MessageItem(str, sdf.format(date));
        if(arrayAdapter != null && dbHelper != null) {
            addItemToDB(item);
            arrayAdapter.add(item);
            arrayAdapter.notifyDataSetChanged();
        }
        else {
            temporaryList.add(item);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        addText("Кофигурация приложения была изменена");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        addText("Приложение было запущено");
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        addText("Память заканчивается, попытка освободить память у приложения");
    }
}
