package com.example.ars.liveapppresentation;

import android.graphics.Color;

class States {
    static int getColorByState(int state)
    {
        switch (state) {
            case 0: return Color.rgb(200, 255 ,185);
            case 1: return Color.rgb(200, 255 ,185);
            case 2: return Color.rgb(200, 255 ,185);
            case 3: return Color.rgb(255, 254 ,184);
            case 4: return Color.parseColor("#FFFFBA86");
            case 5: return Color.parseColor("#FFC8FFB9");
            case 6: return Color.parseColor("#FFD1D1D1");
            default: throw new UnsupportedOperationException("Unknown state");
        }
    }

    static String stateActivityToString(int state)
    {
        switch (state) {
            case 0: return "Create";
            case 1: return "Start";
            case 2: return "Resume";
            case 3: return "Pause";
            case 4: return "Stop";
            case 5: return "Restart";
            case 6: return "Destroy";
            default: throw new UnsupportedOperationException("Unknown state");
        }
    }
}
