package com.example.ars.liveapppresentation;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

class AppArrayAdapter extends ArrayAdapter<MessageItem> {
    private final Context context;
    private final ArrayList<MessageItem> values;

    AppArrayAdapter(Context context, ArrayList<MessageItem> values) {
        super(context, android.R.layout.simple_list_item_2, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MessageItem value = values.get(position);
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(android.R.layout.simple_list_item_2, parent, false);

        TextView stateTextView = (TextView) rowView.findViewById(android.R.id.text1);
        TextView timeTextView = (TextView) rowView.findViewById(android.R.id.text2);

        stateTextView.setText(value.Message);
        timeTextView.setText(value.Time);

        return rowView;
    }
}
