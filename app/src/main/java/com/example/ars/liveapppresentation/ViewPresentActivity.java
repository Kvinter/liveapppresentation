package com.example.ars.liveapppresentation;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class ViewPresentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_present);

        final MyViewGroup layout = (MyViewGroup) findViewById(R.id.frameLayout);
        ListView listView = (ListView) findViewById(R.id.viewPresentList);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, new ArrayList<String>());
        layout.setArrayAdapter(arrayAdapter);
        listView.setAdapter(arrayAdapter);

        final MyEditTextView myView = new MyEditTextView(this, arrayAdapter);
        myView.setText("!!!");
        layout.addView(myView);

        final Button changeTextButton = (Button) findViewById(R.id.changeTextButton);
        changeTextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = myView.getText().toString();
                if(text.equals("Click!"))
                    myView.setText("Yap!");
                else
                    myView.setText("Click!");
            }
        });

        final Button removeViewButton = (Button) findViewById(R.id.removeViewButton);
        removeViewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!changeTextButton.isEnabled()) {
                    layout.addView(myView);
                    removeViewButton.setText("Удалить View");
                    changeTextButton.setEnabled(true);
                }
                else {
                    layout.removeView(myView);
                    removeViewButton.setText("Добавить View");
                    changeTextButton.setEnabled(false);

                }
            }
        });
    }
}
