package com.example.ars.liveapppresentation;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final MyApplication application = getApp();

        Button activityLiveButton = (Button)findViewById(R.id.activityLiveButton);
        Button viewLiveButton = (Button)findViewById(R.id.viewPresentButton);
        Button clearListViewButton = (Button)findViewById(R.id.clearListViewButton);
        activityLiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, LiveActivityActivity.class);
                startActivity(intent);
            }
        });
        viewLiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ViewPresentActivity.class);
                startActivity(intent);
            }
        });
        clearListViewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                application.Clear();
            }
        });
    }

    @NonNull
    public MyApplication getApp() {
        DBHelper dbHelper = new DBHelper(this);
        AppArrayAdapter arrayAdapter = new AppArrayAdapter(this, new ArrayList<MessageItem>());

        ListView listView = (ListView)findViewById(R.id.applicationLifeCircleListView);
        listView.setAdapter(arrayAdapter);
        final MyApplication application = (MyApplication) getApplication();
        application.setArrayAdapter(arrayAdapter);
        application.setDbHelper(dbHelper);
        return application;
    }


}
