package com.example.ars.liveapppresentation;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.KeyEvent;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

class MyEditTextView extends android.support.v7.widget.AppCompatEditText
{
    private ArrayAdapter<String> arrayAdapter;

    public MyEditTextView(Context context, ArrayAdapter<String> arrayAdapter) {
        super(context);
        this.arrayAdapter = arrayAdapter;
    }
    private void addText(String str)
    {
        arrayAdapter.add(str);
        arrayAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        addText("Размер View были расчитаны");
    }


    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        addText("View взято из XML");
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        addText("View прикреплено к окну");
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        addText(String.format("View назначен размер и положение\nisChanged:%b left:%s top:%s right:%s bottom:%s",
                changed, left, top, right, bottom));
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        addText(String.format("Размер View изменен (Old(W:%s H:%s), New(W:%s H:%S))",oldw, oldh, w, h ));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        addText("View отображен");
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        addText(String.format("Произошло событие KeyDown (ketCode:%s KetEventL%S", keyCode, event.toString()));
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        addText(String.format("Произошло событие KeyUp (ketCode:%s KetEventL%S", keyCode, event.toString()));
        return super.onKeyUp(keyCode, event);
    }

    @Override
    protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect);
        addText(String.format("Фокус View изменен на %s", focused ));
    }
}
