package com.example.ars.liveapppresentation;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class LiveActivityActivity extends AppCompatActivity {
    private DBHelper dbHelper;
    private SimpleDateFormat sdf;
    private ListView listView;
    private StateActivityArrayAdapter stateActivityArrayAdapter;

    private void reestablish(SQLiteDatabase db)
    {
        Cursor c = db.query("states", null, null, null, null, null, null);
        if(!c.moveToFirst()) return;
        int statColIndex = c.getColumnIndex("state");
        int timeColIndex = c.getColumnIndex("time");

        do {
            StateActivityItem item = new StateActivityItem(c.getInt(statColIndex), c.getString(timeColIndex));
            stateActivityArrayAdapter.add(item);
        } while(c.moveToNext());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_activity);



        listView = (ListView) findViewById(R.id.statesActivityListView);
        sdf = new SimpleDateFormat("HH:mm:ss.SSS");
        stateActivityArrayAdapter = new StateActivityArrayAdapter(this, new ArrayList<StateActivityItem>());
        listView.setAdapter(stateActivityArrayAdapter);

        dbHelper = new DBHelper(this);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        reestablish(db);

        Button clearButton = (Button) findViewById(R.id.clearListViewButton);
        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stateActivityArrayAdapter.clear();
                stateActivityArrayAdapter.notifyDataSetChanged();
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                db.delete("states", null, null);
            }
        });

        addItem(0);
    }

    @Override
    protected void onStart() {
        super.onStart();
        addItem(1);
    }

    @Override
    protected void onResume() {
        super.onResume();
        addItem(2);
    }

    @Override
    protected void onPause() {
        super.onPause();
        addItem(3);
    }

    @Override
    protected void onStop() {
        super.onStop();
        addItem(4);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        addItem(5);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        addItem(6);
    }

    private void addItem(int state)
    {
        StateActivityItem item = new StateActivityItem(state, sdf.format(new Date()));
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        stateActivityArrayAdapter.add(item);
        stateActivityArrayAdapter.notifyDataSetChanged();

        ContentValues cv = new ContentValues();
        cv.put("state", item.State);
        cv.put("time", item.Time);

        db.insert("states", null, cv);
    }
}
