package com.example.ars.liveapppresentation;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class StateActivityArrayAdapter extends ArrayAdapter<StateActivityItem> {
    private final Context context;
    private final ArrayList<StateActivityItem> values;

    public StateActivityArrayAdapter(Context context, ArrayList<StateActivityItem> values) {
        super(context, R.layout.state_list_view_item, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        StateActivityItem value = values.get(position);
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.state_list_view_item, parent, false);

        int colorByState = States.getColorByState(value.State);
        rowView.setBackgroundColor(colorByState);
        TextView stateTextView = (TextView) rowView.findViewById(R.id.stateTextView);
        TextView timeTextView = (TextView) rowView.findViewById(R.id.timeTextView);

        stateTextView.setText(States.stateActivityToString(value.State));
        timeTextView.setText(value.Time);

        return rowView;
    }
}
